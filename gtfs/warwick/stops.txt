stop_id,stop_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id,stop_url,location_type,parent_station,platform_code
0,900000,Palmerin St at Post Office,,-28.216669,152.032628,1,http://translink.com.au/stop/900000/gtfs/,,,
37,900001,Fitzroy St near Canning St (Hail n Ride),,-28.214600,152.036867,1,http://translink.com.au/stop/900001/gtfs/,,,
39,900004,Bourke St near Grafton Street,,-28.218206,152.044586,1,http://translink.com.au/stop/900004/gtfs/,,,
40,900005,Cecil St near Hamilton St (Hail n Ride),,-28.218690,152.042271,1,http://translink.com.au/stop/900005/gtfs/,,,
41,900006,Pine St near Percy St (Hail n Ride),,-28.220110,152.041808,1,http://translink.com.au/stop/900006/gtfs/,,,
42,900008,Pine St near McEvoy Street,,-28.220959,152.040484,1,http://translink.com.au/stop/900008/gtfs/,,,
43,900009,Mcevoy St near Pratten St (Hail n Ride),,-28.223027,152.040003,1,http://translink.com.au/stop/900009/gtfs/,,,
44,900010,Lyons St near Pratten St (Hail n Ride),,-28.222852,152.038771,1,http://translink.com.au/stop/900010/gtfs/,,,
45,900011,Pratten St near Canning Street,,-28.222733,152.036712,1,http://translink.com.au/stop/900011/gtfs/,,,
46,900012,Canning St near Wood Street,,-28.220709,152.036799,1,http://translink.com.au/stop/900012/gtfs/,,,
47,900013,Canning St near Percy St,,-28.218681,152.037172,1,http://translink.com.au/stop/900013/gtfs/,,,
48,900014,Canning St near Grafton Street,,-28.217641,152.037354,1,http://translink.com.au/stop/900014/gtfs/,,,
1,900015,Pratten St near Albion Street,,-28.222761,152.033571,1,http://translink.com.au/stop/900015/gtfs/,,,
2,900016,Palmerin St near Pratten Street,,-28.222711,152.031544,1,http://translink.com.au/stop/900016/gtfs/,,,
3,900019,Locke St at Assumption College,,-28.225076,152.027658,1,http://translink.com.au/stop/900019/gtfs/,,,
27,900020,Locke St at Abbey Of The Roses,,-28.224931,152.026640,1,http://translink.com.au/stop/900020/gtfs/,,,
4,900021,Locke St near Wantley Street,,-28.224513,152.023837,1,http://translink.com.au/stop/900021/gtfs/,,,
5,900023,Locke St at Warwick Hospital,,-28.223631,152.017618,1,http://translink.com.au/stop/900023/gtfs/,,,
6,900024,Locke St near Deconlay St (Hail n Ride),,-28.223344,152.015673,1,http://translink.com.au/stop/900024/gtfs/,,,
7,900025,Locke St near Clarke Street,,-28.222858,152.012228,1,http://translink.com.au/stop/900025/gtfs/,,,
8,900026,Locke St near Douglas St,,-28.222572,152.010204,1,http://translink.com.au/stop/900026/gtfs/,,,
9,900027,Locke St near Tooth Street,,-28.222228,152.007822,1,http://translink.com.au/stop/900027/gtfs/,,,
10,900028,Redgwell St near Hawthorne St (Hail n Ride),,-28.221882,152.005568,1,http://translink.com.au/stop/900028/gtfs/,,,
11,900029,Redgwell St near Pratten Street,,-28.218844,152.004918,1,http://translink.com.au/stop/900029/gtfs/,,,
12,900030,Alfred St near Miller Street,,-28.217039,152.002199,1,http://translink.com.au/stop/900030/gtfs/,,,
13,900031,Wood St at Akooramak,,-28.214958,152.002839,1,http://translink.com.au/stop/900031/gtfs/,,,
14,900032,Wood St near Wentworth St (Hail n Ride),,-28.215153,152.004038,1,http://translink.com.au/stop/900032/gtfs/,,,
15,900033,Wood St near Wentworth St (Hail n Ride),,-28.216477,152.006073,1,http://translink.com.au/stop/900033/gtfs/,,,
16,900034,Wood St near Mile End Park,,-28.217966,152.008482,1,http://translink.com.au/stop/900034/gtfs/,,,
17,900035,Wood St near Douglas St (Hail n Ride),,-28.218246,152.010490,1,http://translink.com.au/stop/900035/gtfs/,,,
18,900036,Wood St at Westside Shopping Centre,,-28.217767,152.014050,1,http://translink.com.au/stop/900036/gtfs/,,,
19,900037,Wood St near Crawford St (Hail n Ride),,-28.218151,152.016713,1,http://translink.com.au/stop/900037/gtfs/,,,
20,900038,Wood St near Short Street,,-28.218400,152.019219,1,http://translink.com.au/stop/900038/gtfs/,,,
21,900039,Wood St near Gore St (Hail n Ride),,-28.218934,152.022922,1,http://translink.com.au/stop/900039/gtfs/,,,
22,900040,Wood St near Wantley St (Hail n Ride),,-28.219277,152.025182,1,http://translink.com.au/stop/900040/gtfs/,,,
23,900041,Dragon St at Warwick Medical Centre,,-28.219246,152.027194,1,http://translink.com.au/stop/900041/gtfs/,,,
24,900042,Dragon St near Percy St (Hail n Ride),,-28.217994,152.027420,1,http://translink.com.au/stop/900042/gtfs/,,,
25,900043,Dragon St near Grafton Street,,-28.215761,152.027827,1,http://translink.com.au/stop/900043/gtfs/,,,
26,900044,Grafton St near Guy St (Hail n Ride),,-28.215724,152.030008,1,http://translink.com.au/stop/900044/gtfs/,,,
49,900046,Albert St at Warwick Croquet Club (Hail n Ride),,-28.211886,152.031318,1,http://translink.com.au/stop/900046/gtfs/,,,
50,900047,Dragon St near Albert Street,,-28.211068,152.028693,1,http://translink.com.au/stop/900047/gtfs/,,,
51,900048,Rosehill Rd near Victoria Street,,-28.207570,152.021629,1,http://translink.com.au/stop/900048/gtfs/,,,
52,900052,Yarrawonga St near Evans Cr,,-28.203458,152.023061,1,http://translink.com.au/stop/900052/gtfs/,,,
53,900053,Glennie St near Horsman Road,,-28.202828,152.026638,1,http://translink.com.au/stop/900053/gtfs/,,,
54,900054,Gertrude St near Horsman Road,,-28.201702,152.027186,1,http://translink.com.au/stop/900054/gtfs/,,,
55,900055,Elizabeth St at Glennie Heights Prep,,-28.199731,152.028965,1,http://translink.com.au/stop/900055/gtfs/,,,
56,900056,Weewondilla Road near Hawker Rd,,-28.197715,152.030772,1,http://translink.com.au/stop/900056/gtfs/,,,
57,900057,Golf Links Ave near Fairway Drive,,-28.196311,152.031937,1,http://translink.com.au/stop/900057/gtfs/,,,
58,900058,Ogilvie Rd at Glengallan,,-28.194167,152.037445,1,http://translink.com.au/stop/900058/gtfs/,,,
59,900059,Glengallan Rd near Ogilvie Road,,-28.195247,152.042750,1,http://translink.com.au/stop/900059/gtfs/,,,
60,900060,Horsman Rd near Glengallan Road,,-28.199206,152.040225,1,http://translink.com.au/stop/900060/gtfs/,,,
61,900061,Horsman Rd near Steel Street,,-28.200494,152.036441,1,http://translink.com.au/stop/900061/gtfs/,,,
62,900062,Horsman Rd near Weewondilla Road,,-28.202531,152.030381,1,http://translink.com.au/stop/900062/gtfs/,,,
63,900063,Park Rd at Queens Park,,-28.206775,152.033554,1,http://translink.com.au/stop/900063/gtfs/,,,
64,900064,Palmerin St near Albert St (Hail n Ride),,-28.212440,152.033364,1,http://translink.com.au/stop/900064/gtfs/,,,
28,900065,Willi St near Aldred St (Hail n Ride),,-28.218348,152.001809,1,http://translink.com.au/stop/900065/gtfs/,,,
29,900066,Diery St near Matthew Jones Drive,,-28.220261,151.999494,1,http://translink.com.au/stop/900066/gtfs/,,,
30,900067,Diery St at Little Warner Street,,-28.222783,151.999031,1,http://translink.com.au/stop/900067/gtfs/,,,
31,900068,Warner St near North Street,,-28.220617,151.987619,1,http://translink.com.au/stop/900068/gtfs/,,,
32,900069,Lyndhurst Lane near Robyn St,,-28.217607,151.985483,1,http://translink.com.au/stop/900069/gtfs/,,,
33,900070,Cunningham Hwy at Griffith Estate,,-28.213056,151.989331,1,http://translink.com.au/stop/900070/gtfs/,,,
34,900071,Cunningham Hwy near Parker St (Hail n Ride),,-28.213997,151.996005,1,http://translink.com.au/stop/900071/gtfs/,,,
35,900072,Pratten St near George Street,,-28.219997,152.015387,1,http://translink.com.au/stop/900072/gtfs/,,,
36,900073,Pratten St near Gore St,,-28.220908,152.021813,1,http://translink.com.au/stop/900073/gtfs/,,,
65,900075,Ann St near Bertram Street,,-28.224758,152.013689,1,http://translink.com.au/stop/900075/gtfs/,,,
66,900076,Cunningham St near Leslie St (Hail n Ride),,-28.227488,152.013481,1,http://translink.com.au/stop/900076/gtfs/,,,
67,900077,Baguley St near Margaret Street,,-28.228562,152.009911,1,http://translink.com.au/stop/900077/gtfs/,,,
68,900078,Margaret St near Cullen St (Hail n Ride),,-28.232141,152.008860,1,http://translink.com.au/stop/900078/gtfs/,,,
69,900079,Cullen Street West near Sandstone Ct,,-28.232046,152.007033,1,http://translink.com.au/stop/900079/gtfs/,,,
70,900080,Bracker Rd near Bisley Street,,-28.236261,152.006534,1,http://translink.com.au/stop/900080/gtfs/,,,
71,900081,Bracker Rd near Rosenthal Road,,-28.237754,152.016254,1,http://translink.com.au/stop/900081/gtfs/,,,
72,900082,Bracker Rd near Kidman Dr (Hail n Ride),,-28.238421,152.021499,1,http://translink.com.au/stop/900082/gtfs/,,,
73,900083,Dragon St at TAFE College,,-28.233362,152.024646,1,http://translink.com.au/stop/900083/gtfs/,,,
74,900084,Wallace St near Aspinall Street,,-28.227405,152.025715,1,http://translink.com.au/stop/900084/gtfs/,,,
75,900085,Dragon St near Locke St (Hail n Ride),,-28.224513,152.026216,1,http://translink.com.au/stop/900085/gtfs/,,,
38,900086,Haidleys Depot (Lyons St),,-28.217509,152.039960,1,http://translink.com.au/stop/900086/gtfs/,,,
