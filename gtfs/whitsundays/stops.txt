stop_id,stop_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id,stop_url,location_type,parent_station,platform_code
0,710000,Shute Harbour Rd at Orchid Road,,-20.301317,148.668297,3,http://translink.com.au/stop/710000/gtfs/,,,
1,710001,Shute Harbour Rd near Orchid Rd (Hail n Ride),,-20.299124,148.669863,3,http://translink.com.au/stop/710001/gtfs/,,,
29,710002,Paluma Rd at Whitsunday Plaza,,-20.289183,148.674606,3,http://translink.com.au/stop/710002/gtfs/,,,
2,710003,Shute Harbour Rd at Adv Whitsunday,,-20.287166,148.678895,3,http://translink.com.au/stop/710003/gtfs/,,,
3,710004,Shute Harbour Rd at William Murray Drive,,-20.285810,148.681922,3,http://translink.com.au/stop/710004/gtfs/,,,
4,710005,Shute Harbour Rd at Macarthur Dr,,-20.284491,148.685369,3,http://translink.com.au/stop/710005/gtfs/,,,
5,710006,Shute Harbour Rd at Manooka Drive,,-20.280828,148.690279,3,http://translink.com.au/stop/710006/gtfs/,,,
6,710007,Shute Harbour Rd at Pandanus Drive,,-20.276968,148.694392,1,http://translink.com.au/stop/710007/gtfs/,,,
7,710008,Whitsunday Shopping Centre,,-20.278288,148.699897,1,http://translink.com.au/stop/710008/gtfs/,,,
59,710009,Eshelby Dr near Island Dr (Hail n Ride),,-20.278681,148.700386,1,http://translink.com.au/stop/710009/gtfs/,,,
42,710011,Coral Esp near Beach Rd (Hail n Ride),,-20.273478,148.697907,1,http://translink.com.au/stop/710011/gtfs/,,,
8,710013,Shute Harbour Rd near Altmann Ave (Hail n Ride),,-20.273885,148.703297,1,http://translink.com.au/stop/710013/gtfs/,,,
9,710014,Shute Harbour Rd at Altmann Avenue,,-20.273128,148.704486,1,http://translink.com.au/stop/710014/gtfs/,,,
10,710016,Coral Sea Marina,,-20.266979,148.711547,1,http://translink.com.au/stop/710016/gtfs/,,,
43,710017,Broadwater Ave near Hill Crest Ave (Hail n Ride),,-20.264272,148.716086,1,http://translink.com.au/stop/710017/gtfs/,,,
11,710018,Shute Harbour Rd at Airlie Beach Central,,-20.268281,148.717710,1,http://translink.com.au/stop/710018/gtfs/,,,
14,710020,Shute Harbour Rd near Port Dr (Hail n Ride),,-20.274499,148.724370,1,http://translink.com.au/stop/710020/gtfs/,,,
16,710021,Shute Harbour Rd at Plantation Drive,,-20.277759,148.726895,1,http://translink.com.au/stop/710021/gtfs/,,,
17,710022,Shute Harbour Rd at Loop Road,,-20.281041,148.731064,1,http://translink.com.au/stop/710022/gtfs/,,,
18,710023,Shute Harbour Rd at Island Rd Island Traders,,-20.281451,148.734638,2,http://translink.com.au/stop/710023/gtfs/,,,
19,710024,Shute Harbour Rd at Ferntree Road,,-20.283320,148.738846,2,http://translink.com.au/stop/710024/gtfs/,,,
20,710025,Shute Harbour Rd at Mandalay Road,,-20.283479,148.741917,2,http://translink.com.au/stop/710025/gtfs/,,,
21,710026,Shute Harbour Rd near Jasinique Dr,,-20.271605,148.749750,2,http://translink.com.au/stop/710026/gtfs/,,,
22,710027,Shute Harbour Rd at Flametree,,-20.276641,148.752924,2,http://translink.com.au/stop/710027/gtfs/,,,
23,710028,Shute Harbour Rd at Air Whitsunday Road,,-20.278977,148.754561,2,http://translink.com.au/stop/710028/gtfs/,,,
24,710029,Shute Harbour Rd at Conway Picnic,,-20.284208,148.763940,2,http://translink.com.au/stop/710029/gtfs/,,,
25,710030,Shute Harbour Rd at Conway NP,,-20.286031,148.771122,2,http://translink.com.au/stop/710030/gtfs/,,,
26,710031,Shute Harbour Rd (Hail n Ride),,-20.289901,148.781082,2,http://translink.com.au/stop/710031/gtfs/,,,
27,710032,Shute Harbour Dr near Whitsunday Dr (Hail n Ride),,-20.290373,148.784164,2,http://translink.com.au/stop/710032/gtfs/,,,
28,710033,Shute Harbour,,-20.293065,148.785562,2,http://translink.com.au/stop/710033/gtfs/,,,
44,710034,Shute Harbour Rd near Whitsunday Dr (Hail n Ride),,-20.291151,148.782689,2,http://translink.com.au/stop/710034/gtfs/,,,
45,710035,Shute Harbour Rd at Conway NP,,-20.286331,148.770854,2,http://translink.com.au/stop/710035/gtfs/,,,
46,710036,Shute Harbour Rd at Conway Picnic,,-20.284107,148.762781,2,http://translink.com.au/stop/710036/gtfs/,,,
47,710037,Shute Harbour Rd at Air Whitsunday Road,,-20.278612,148.754093,2,http://translink.com.au/stop/710037/gtfs/,,,
48,710038,Shute Harbour Rd at Flametree,,-20.275895,148.752273,2,http://translink.com.au/stop/710038/gtfs/,,,
49,710039,Shute Harbour Rd near Jasinique Dr (Hail n Ride),,-20.271494,148.749140,2,http://translink.com.au/stop/710039/gtfs/,,,
50,710040,Shute Harbour Rd at Mandalay Road,,-20.283600,148.741396,2,http://translink.com.au/stop/710040/gtfs/,,,
51,710041,Shute Harbour Rd at Ferntree Road,,-20.283452,148.738104,2,http://translink.com.au/stop/710041/gtfs/,,,
52,710042,Shute Harbour Rd at Cedar Crescent,,-20.281953,148.735412,2,http://translink.com.au/stop/710042/gtfs/,,,
53,710043,Shute Harbour Rd at Loop Road,,-20.281194,148.730641,1,http://translink.com.au/stop/710043/gtfs/,,,
54,710044,Shute Harbour Rd at Plantation Drive,,-20.277796,148.726615,1,http://translink.com.au/stop/710044/gtfs/,,,
55,710045,Shute Harbour Rd at Hermitage Dr,,-20.275188,148.724475,1,http://translink.com.au/stop/710045/gtfs/,,,
57,710047,Shute Harbour Rd near Broadwater Avenue,,-20.268078,148.716783,1,http://translink.com.au/stop/710047/gtfs/,,,
58,710048,Shute Harbour Rd at Saint Martins Lane,,-20.273932,148.703507,1,http://translink.com.au/stop/710048/gtfs/,,,
60,710050,Shute Harbour Rd at Stewart Drive,,-20.277158,148.694811,1,http://translink.com.au/stop/710050/gtfs/,,,
61,710051,Shute Harbour Rd at Manooka Drive,,-20.281349,148.690274,3,http://translink.com.au/stop/710051/gtfs/,,,
62,710052,Shute Harbour Rd at Willis Court,,-20.284938,148.684917,3,http://translink.com.au/stop/710052/gtfs/,,,
63,710053,Shute Harbor Rd near William Murray Dr (HnR),,-20.286031,148.681831,3,http://translink.com.au/stop/710053/gtfs/,,,
64,710054,Shute Harbour Rd near Orchid Rd (Hail n Ride),,-20.299449,148.669809,3,http://translink.com.au/stop/710054/gtfs/,,,
65,710055,Shute Harbour Rd near Orchid Rd (Hail n Ride),,-20.301415,148.668428,3,http://translink.com.au/stop/710055/gtfs/,,,
66,710056,Shute Harbour Rd near Stanley Dr (Hail n Ride),,-20.304181,148.665078,3,http://translink.com.au/stop/710056/gtfs/,,,
67,710057,Shute Harbour Rd near Kookaburra Dr (Hail n Ride),,-20.308276,148.660157,3,http://translink.com.au/stop/710057/gtfs/,,,
68,710058,Shute Harbour Rd near Greg Cannon Valley Rd (HnR),,-20.323290,148.653230,4,,,,
69,710059,Shute Harbour Rd at Brandy Creek Road,,-20.333928,148.649115,4,http://translink.com.au/stop/710059/gtfs/,,,
70,710060,Shute Harbour Rd at Rifle Range Road,,-20.345216,148.641958,4,http://translink.com.au/stop/710060/gtfs/,,,
71,710062,Shute Harbour Rd near Jansen Rd (Hail n Ride),,-20.344658,148.630328,4,http://translink.com.au/stop/710062/gtfs/,,,
72,710063,Camm Rd near Usher Road,,-20.374572,148.621195,5,http://translink.com.au/stop/710063/gtfs/,,,
73,710064,Shute Harbour Rd near Braemar Rd (Hail n Ride),,-20.382612,148.610332,5,http://translink.com.au/stop/710064/gtfs/,,,
74,710065,Shute Harbour Rd near Tyree Rd (Hail n Ride),,-20.379704,148.597029,6,http://translink.com.au/stop/710065/gtfs/,,,
30,710066,Blair Street at Main St,,-20.401790,148.586802,6,http://translink.com.au/stop/710066/gtfs/,,,
31,710070,Mill St at Proserpine,,-20.400413,148.581585,6,http://translink.com.au/stop/710070/gtfs/,,,
32,710071,Taylor St near Herbert St (Hail n Ride),,-20.399417,148.585111,6,http://translink.com.au/stop/710071/gtfs/,,,
33,710072,Shute Harbour Rd at Tyree Road,,-20.379555,148.597067,6,http://translink.com.au/stop/710072/gtfs/,,,
34,710073,Shute Harbour Rd at Braemar Road,,-20.382470,148.610429,5,http://translink.com.au/stop/710073/gtfs/,,,
36,710074,Shute Harbour Rd at Plemenuk Road,,-20.343872,148.632732,4,http://translink.com.au/stop/710074/gtfs/,,,
38,710075,Shute Harbour Rd at Brandy Creek Road,,-20.333816,148.648977,4,http://translink.com.au/stop/710075/gtfs/,,,
39,710076,Shute Harbour Rd near Greg Cannon Valley Rd (HnR),,-20.322649,148.653333,4,,,,
40,710077,Shute Harbour Rd near Riordanvale Rd (Hail n Ride),,-20.308182,148.660025,3,http://translink.com.au/stop/710077/gtfs/,,,
35,710079,Camm Rd near Shute Harbour Rd (Hail n Ride),,-20.374329,148.621317,5,http://translink.com.au/stop/710079/gtfs/,,,
37,710080,Shute Harbour Rd near Rifle Range Rd (Hail n Ride),,-20.344942,148.640926,4,http://translink.com.au/stop/710080/gtfs/,,,
13,710088,Shute Harbour Rd at Hermitage Drive,,-20.272890,148.723206,1,http://translink.com.au/stop/710088/gtfs/,,,
75,710089,Ruge St near Chapman St (Hail n Ride),,-20.406011,148.582407,6,http://translink.com.au/stop/710089/gtfs/,,,
76,710090,Anzac St near Nicol St (Hail n Ride),,-20.404236,148.573560,6,http://translink.com.au/stop/710090/gtfs/,,,
41,710091,Beach Rd near Groper Lane (Hail n Ride),,-20.276329,148.696434,1,http://translink.com.au/stop/710091/gtfs/,,,
15,710092,Port of Airlie,,-20.272564,148.724885,1,http://translink.com.au/stop/710092/gtfs/,,,
12,710093,Coconut Grove at Airlie Esplanade,,-20.269730,148.721594,1,http://translink.com.au/stop/710093/gtfs/,,,
56,710094,Coconut Grove at Airlie Esplanade,,-20.269681,148.721531,1,http://translink.com.au/stop/710094/gtfs/,,,
