route_id,service_id,trip_id,trip_headsign,direction_id,block_id,shape_id
0,3,0,Velution Street,1,,0
0,3,1,Central Arcade,0,,1
0,3,2,Central Arcade,0,,2
0,3,3,Central Arcade,0,,1
0,3,4,Warrina Home,1,,3
1,3,5,Central Arcade,1,,4
1,3,6,Innifail Estate,0,,5
3,3,7,Central Arcade,1,,6
3,3,8,Central Arcade,1,,6
4,3,9,Flying Fish Point,1,,7
4,3,10,Central Arcade,0,,8
4,3,11,Central Arcade,0,,8
4,3,12,Flying Fish Point,1,,7
2,3,13,Central Arcade,0,,9
2,3,14,Central Arcade,0,,9
2,3,15,Central Arcade,0,,9
2,3,16,Central Arcade,0,,9
5,3,17,Cairns Central,0,,10
5,3,18,Atherton,1,,11
5,3,19,Cairns Central,0,,10
5,3,20,Atherton,1,,11
5,3,21,Cairns Central,0,,10
6,3,22,Kuranda,1,,12
5,3,23,Atherton,1,,11
6,3,24,Cairns Central,0,,13
0,1,25,Central Arcade,0,,1
0,1,26,Central Arcade,0,,2
0,1,27,Warrina Home,1,,3
1,1,28,Innifail Estate,0,,5
1,1,29,Central Arcade,1,,4
1,1,30,Central Arcade,1,,4
1,1,31,Innifail Estate,0,,5
2,1,32,Central Arcade,0,,9
2,1,33,Central Arcade,0,,9
3,1,34,Goondi State School,1,,14
3,1,35,Central Arcade,1,,15
4,1,36,Flying Fish Point,1,,16
4,1,37,Central Arcade,0,,17
4,1,38,Central Arcade,0,,17
4,1,39,Flying Fish Point,1,,16
5,1,40,Atherton,1,,11
5,1,41,Atherton,1,,11
5,1,42,Cairns Central,0,,10
5,1,43,Cairns Central,0,,10
5,2,44,Cairns Central,0,,10
5,2,45,Cairns Central,0,,10
5,2,46,Atherton,1,,11
5,2,47,Atherton,1,,11
0,4,48,Velution Street,1,,0
0,4,49,Central Arcade,0,,1
0,4,50,Central Arcade,0,,2
0,4,51,Central Arcade,0,,1
0,4,52,Warrina Home,1,,3
1,4,53,Central Arcade,1,,4
1,4,54,Innifail Estate,0,,5
3,4,55,Central Arcade,1,,6
3,4,56,Central Arcade,1,,6
4,4,57,Flying Fish Point,1,,7
4,4,58,Central Arcade,0,,8
4,4,59,Central Arcade,0,,8
4,4,60,Flying Fish Point,1,,7
2,4,61,Central Arcade,0,,9
2,4,62,Central Arcade,0,,9
2,4,63,Central Arcade,0,,9
2,4,64,Central Arcade,0,,9
5,4,65,Atherton,1,,11
5,4,66,Atherton,1,,11
5,4,67,Atherton,1,,11
5,4,68,Cairns Central,0,,18
5,4,69,Cairns Central,0,,10
5,4,70,Cairns Central,0,,10
6,4,71,Kuranda,1,,12
6,4,72,Cairns Central,0,,13
