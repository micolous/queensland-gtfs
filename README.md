# Queensland (Translink & qConnect) GTFS mirror

This allows us to keep the GTFS data from Queensland Department of Transport
and Main Roads in a revision control system (so we could look up historical
data).

It should be trivial to adapt this to monitor another public transit system.

This is best used in a daily crontab.

You shouldn't need to run this script yourself though, I already provide a
repository at https://bitbucket.org/micolous/queensland-gtfs.  It has data
starting from April 2015.  However, qconnect (regional) feed data is missing
between June 2016 and October 2018, due to an unnoticed URL change by the
Department of Transport and Main Roads.

## Data information

The data provided (in the `gtfs` folder) is licensed under [Creative Commons
Australia Attribution 4.0
License](http://creativecommons.org/licenses/by/4.0/au/deed.en).

Subfolders exist for every one of the source feeds.  Data for the greater
Brisbane metro area can be found in the `gtfs/SEQ` folder.

This repository uses the tool
[shittypack](https://github.com/micolous/shittypack) in order to
automatically reformat the contents of this dataset.  This generally
improves the quality of the source dataset by removing redundant, useless or
verbose data.  However, it is done without any human quality analysis, and
may be subject to additional errors not in the source data.

This work is based on data from the Queensland Department of Transport and Main
Roads: [Translink](https://data.qld.gov.au/dataset/general-transit-feed-specification-gtfs-seq)
and [qConnect](https://data.qld.gov.au/dataset/general-transit-feed-specification-gtfs-qconnect).

## mirror.sh

The mirror script is licensed under the following terms (3-clause BSD):

Copyright 2011, 2014, 2015, 2018 Michael Farrell <http://micolous.id.au>

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

* The names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
