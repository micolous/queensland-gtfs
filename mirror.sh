#!/bin/bash
# GTFS mirroring tool.  This allows us to keep the GTFS data from Translink and
# qConnect into a revision control system (so we could look up historical data).
#
# Copyright 2011, 2014, 2015, 2018 Michael Farrell <http://micolous.id.au>
#
# This software is licensed under a 3-clause BSD license, see the README.md
# file for more details.

DATA_ZIP="https://gtfsrt.api.translink.com.au/GTFS"

# Create the target folder
mkdir -p "gtfs"

# Queensland data is in several parts, need to grab each...
for region_code in whitsundays:WHT bowen:BOW bundaberg:BUN cairns:CNS gladstone:GLT gympie:GYM innisfail:INN kilcoy:KIL mackay:MKY magnetic-island:MAG maleny-landsborough:MAL maryborough-herveybay:MHB rockhampton:ROK toowoomba:TWB townsville:TSV warwick:WAR yeppoon:YEP magnetic-island-ferry:MIF SEQ:SEQ
do
	region="${region_code%:*}"
	code="${region_code#*:}"
	mkdir -p "gtfs/${region}"
	wget -N "${DATA_ZIP}/${code}_GTFS.zip"

	# While shittypack is only really needed for SEQ, run it on all files.
	python shittypack/shittypack.py -o "gtfs/${region}/shittypack.zip" "${code}_GTFS.zip"

	unzip -ouL "gtfs/${region}/shittypack.zip" -d "gtfs/${region}"
	git add gtfs/${region}/*.txt
done

# Commit
DATE="`LANG=C TZ=UTC date`"
MSG="New data from upstream source on ${DATE}"

git commit -am "${MSG}"

# Push downstream to bitbucket
git push origin master

